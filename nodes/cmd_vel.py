#!/usr/bin/env python

from geometry_msgs.msg import Twist
import rospy

from math import pi
import sys

from std_msgs.msg import String

global cmd_msg
global linear
global angular


def key_listener(msg):
    global cmd_msg
    global linear
    global angular
    # print ("Key listener %s" % msg)

    # if name == "robot1":
    #
    # elif name == "robot2":
    #     changes = {"w": (1.0, 0.0), "s": (-1.0, 0.0), "a": (0.0, 0.3*pi), "d": (0.0, -0.3*pi), "stop": (0.0, 0.0)}

    changes = {"up": (1.0, 0.0), "down": (-1.0, 0.0), "left": (0.0, 0.3 * pi), "right": (0.0, -0.3 * pi),
               "stop": (0.0, 0.0)}
    try:
        change = changes[msg.data]
    except Exception as e:
        return

    cmd_msg.linear.x = change[0]
    cmd_msg.angular.z = change[1]

    pub.publish(cmd_msg)


if __name__ == '__main__':
    _, name = sys.argv[1].split("=")
    print("robot1node name %s" % name)
    rospy.init_node("robotnode%s" % name)
    sub = rospy.Subscriber('/' + name + "/key", String, key_listener)
    pub = rospy.Publisher('/' + name + "/cmd_vel/", Twist, queue_size=10)
    cmd_msg = Twist()
    linear = 0.0
    angular = 0.0
    while ~rospy.is_shutdown():
        rospy.spin()

