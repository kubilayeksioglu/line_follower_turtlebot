#!/usr/bin/env python

import rospy
import sys
from std_msgs.msg import String
import sys, select, os
import tty, termios

pub = None
msg = None
name = None


def on_press(key):
    # print("Client pressed %s %s" % (name, key))
    map_2 = {"w": "up", "s": "down", "d": "right", "a": "left", "": "stop"}
    change = map_2.get(key)
    if change is not None:
        msg.data = change
        pub.publish(msg)
    

def getKey():
    tty.setraw(sys.stdin.fileno())
    rlist, _, _ = select.select([sys.stdin], [], [], 0.1)
    if rlist:
        key = sys.stdin.read(1)
    else:
        key = ''

    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


if __name__ == '__main__':
    _, name = sys.argv[1].split("=")
    print("key listener1 name %s" % name)
    rospy.init_node("client%s" % name)
    pub = rospy.Publisher('/' + name + "/key", String, queue_size=10)
    msg = String()
    if os.name != 'nt':
        settings = termios.tcgetattr(sys.stdin)
    while(1):
        key = getKey()
        if (key == '\x03'):
            break
        on_press(key)  

